#ifndef DOLLAR_HPP
#define DOLLAR_HPP

class Dollar
{
public:
    static int amount;
    Dollar(int amount);
    void times(int multiplier);
};

#endif // DOLLAR_HPP
